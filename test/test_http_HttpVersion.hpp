#pragma once

#include <string>

#include <gtest/gtest.h>

#include "../src/http/HttpVersion.hpp"

using namespace echttpd;

TEST(http_HttpVersion, ParseRender)
{
	ASSERT_EQ(http::httpVersionFromString("HTTP/1.0"), http::HttpVersion::v1_0);
	ASSERT_EQ(http::httpVersionToString(http::HttpVersion::v1_1), "HTTP/1.1");
}
