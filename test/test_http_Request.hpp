#pragma once

#include <string>
#include <sstream>

#include <gtest/gtest.h>

#include "../src/http/Request.hpp"

using namespace echttpd;

TEST(http_Request, Parse)
{
	std::stringstream raw_request_ss;
	raw_request_ss << "GET / HTTP/1.1\r\n"
			       << "Header1: Value1\n"
				   << "Header2: Value2a:Value2b\n"
				   << "\r\n";

	auto request = http::Request::fromString(raw_request_ss.str());

	ASSERT_EQ(request.getMethod(), http::HttpMethod::GET);
	ASSERT_EQ(request.getHttpVersion(), http::HttpVersion::v1_1);
	ASSERT_EQ(request.getURI(), "/");
	ASSERT_TRUE(request.hasHeader("Header1"));
	ASSERT_TRUE(request.hasHeader("Header2"));
	ASSERT_FALSE(request.hasHeader("Header3"));
	ASSERT_EQ(request.getHeader("Header1"), "Value1");
	ASSERT_EQ(request.getHeader("Header2"), "Value2a:Value2b");
	ASSERT_TRUE(request.isValid());
}
