#pragma once

#include <string>
#include <vector>

#include <gtest/gtest.h>

#include "../src/StringUtils.hpp"

using namespace echttpd;

typedef std::vector<std::string> V;

TEST(StringUtils, Split)
{
	ASSERT_EQ(StringUtils::split("", ','), (V { }));
	ASSERT_EQ(StringUtils::split("a", ','), (V { "a" }));
	ASSERT_EQ(StringUtils::split("a,b,c", ','), (V { "a", "b", "c" }));
	ASSERT_EQ(StringUtils::split("a,b,c", ',', 1), (V { "a", "b,c" }));
	ASSERT_EQ(StringUtils::split(",a,b,c,", ','), (V { "", "a", "b", "c" }));
	ASSERT_EQ(StringUtils::split(",,,", ','), (V { "", "", "" }));
}

TEST(StringUtils, TrimLastIf)
{
	ASSERT_EQ(StringUtils::trimLastIf("", 'b'), "");
	ASSERT_EQ(StringUtils::trimLastIf("a", 'b'), "a");
	ASSERT_EQ(StringUtils::trimLastIf("ab", 'b'), "a");
	ASSERT_EQ(StringUtils::trimLastIf("abb", 'b'), "ab");
}
