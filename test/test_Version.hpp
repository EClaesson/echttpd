#pragma once

#include <gtest/gtest.h>

#include "../src/Version.hpp"

using namespace echttpd;

TEST(Version, GetSet)
{
	auto version = Version(1, 0, 23);

	ASSERT_EQ(version.getMajor(), 1);
	ASSERT_EQ(version.getMinor(), 0);
	ASSERT_EQ(version.getPatch(), 23);
	ASSERT_EQ(version.getAsString(), "1.0.23");
}

TEST(Version, Comparison)
{
	ASSERT_TRUE(Version(1, 2, 3) == Version(1, 2, 3));
	ASSERT_TRUE(Version(1, 2, 3) != Version(2, 3, 4));

	ASSERT_TRUE(Version(1, 2, 3) < Version(2, 3, 4));
	ASSERT_TRUE(Version(1, 2, 3) < Version(1, 3, 4));
	ASSERT_TRUE(Version(1, 2, 3) < Version(1, 2, 4));
	ASSERT_TRUE(Version(1, 2, 3) <= Version(2, 3, 4));
	ASSERT_TRUE(Version(1, 2, 3) <= Version(1, 2, 3));

	ASSERT_TRUE(Version(2, 3, 4) > Version(1, 2, 3));
	ASSERT_TRUE(Version(1, 3, 4) > Version(1, 2, 3));
	ASSERT_TRUE(Version(1, 2, 4) > Version(1, 2, 3));
	ASSERT_TRUE(Version(2, 3, 4) >= Version(1, 2, 3));
	ASSERT_TRUE(Version(1, 2, 3) >= Version(1, 2, 3));
}
