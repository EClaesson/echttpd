#pragma once

#include <string>

#include <gtest/gtest.h>

#include "../src/http/Response.hpp"

using namespace echttpd;

TEST(http_Response, Response)
{
	auto request = http::Request(http::HttpMethod::GET, "/", http::HttpVersion::v1_1, { });
	auto response = http::Response(request, 200, "BODY");

	std::string expected_data = "HTTP/1.1 200 OK\n\n";

	ASSERT_EQ(response.getStatusCode(), 200);
	ASSERT_EQ(response.getStatusMessage(), "OK");
	ASSERT_EQ(response.getHttpVersion(), request.getHttpVersion());
	ASSERT_EQ(response.getBody(), "BODY");
	ASSERT_EQ(response.toStringData(), expected_data);
}
