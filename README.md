## echttpd

A simple HTTP server.

### Configuration

echttpd takes no command line parameters, but basic configuration can be done in the HttpServer constructor call in main.cpp.

    server(bind_address, port, content_root)
    
If compiled with DEBUG defined, debug logging will be enabled.

If compiled with ECHTTPD_TEST, the test suite will be built instead of the server.