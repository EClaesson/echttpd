#pragma once

#include <string>
#include <vector>

namespace echttpd
{
	/**
	 * \brief Collection of string utilities.
	 */
	class StringUtils
	{
	public:
		/**
		 * \brief Split elements of a string by a delimiter.
		 * \param[in] str The string to split.
		 * \param[in] delimiter The delimiter to split at.
		 * \param[in] max_splits Maximum number of splits, the remaining elements will be joined.
		 */
		static std::vector<std::string> split(const std::string& str, const char delimiter, const unsigned int max_splits = 0);

		/**
		 * \brief Removes a the last character if it is a specific character.
		 * \param[in] str The string to trim.
		 * \param[in] chr The character to remove.
		 */
		static std::string trimLastIf(const std::string& str, const char chr);
	};
}
