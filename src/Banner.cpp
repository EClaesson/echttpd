#include "Banner.hpp"

#include <sstream>

echttpd::Banner::Banner(const std::string& text, const char border, const unsigned short cols) : m_text(text), m_border_char(border), m_cols(cols)
{

}

std::string echttpd::Banner::getAsString() const
{
	std::stringstream ss;

	ss << std::string(m_cols, m_border_char) << "\n"
	   << m_border_char << " " << m_text << std::string(m_cols - m_text.length() - 3, ' ') << "#\n"
	   << std::string(m_cols, m_border_char) << "\n";

	return ss.str();
}
