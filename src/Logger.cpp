#include "Logger.hpp"

#include <ctime>
#include <iostream>
#include <iomanip>

void echttpd::Logger::mm_output(const std::string& title, const std::string& msg) const
{
	auto time = std::time(nullptr);
	auto tm = *std::localtime(&time);

	std::cout << std::put_time(&tm, "%Y-%m-%d %H:%M:%S") << " [" << m_name << "] " << title << ": " << msg << std::endl;
}

echttpd::Logger::Logger() : m_name("")
{

}

echttpd::Logger::Logger(const std::string& name) : m_name(name)
{

}

void echttpd::Logger::debug(const std::string& msg) const
{
#ifdef DEBUG
	mm_output("DEBUG", msg);
#endif
}

void echttpd::Logger::info(const std::string& msg) const
{
	mm_output("INFO", msg);
}

void echttpd::Logger::warning(const std::string& msg) const
{
	mm_output("WARNING", msg);
}

void echttpd::Logger::error(const std::string& msg) const
{
	mm_output("ERROR", msg);
}

void echttpd::Logger::fatal(const std::string& msg) const
{
	mm_output("FATAL", msg);
	exit(1);
}

echttpd::Logger echttpd::Logger::getLogger(const std::string& name)
{
	return Logger(name);
}
