#include "Version.hpp"

#include <sstream>

echttpd::Version echttpd::Version::current = echttpd::Version(0, 0, 1);

echttpd::Version::Version(int major, int minor, int patch)
{
	m_major = major;
	m_minor = minor;
	m_patch = patch;
}

int echttpd::Version::getMajor() const { return m_major; }
int echttpd::Version::getMinor() const { return m_minor; }
int echttpd::Version::getPatch() const { return m_patch; }

std::string echttpd::Version::getAsString() const
{
	std::stringstream ss;
	ss << m_major << "." << m_minor << "." << m_patch;
	return ss.str();
}

bool echttpd::Version::operator==(const Version& other)
{
	return m_major == other.m_major &&
		   m_minor == other.m_minor &&
		   m_patch == other.m_patch;
}

bool echttpd::Version::operator!=(const Version& other)
{
	return !(*this == other);
}

bool echttpd::Version::operator<(const Version& other)
{
	return (m_major < other.m_major) ||
		   (m_major == other.m_major && m_minor < other.m_minor) ||
		   (m_major == other.m_major && m_minor == other.m_minor && m_patch < other.m_patch);
}

bool echttpd::Version::operator<=(const Version& other)
{
	return *this < other || *this == other;
}

bool echttpd::Version::operator>(const Version& other)
{
	return (m_major > other.m_major) ||
		   (m_major == other.m_major && m_minor > other.m_minor) ||
		   (m_major == other.m_major && m_minor == other.m_minor && m_patch > other.m_patch);
}

bool echttpd::Version::operator>=(const Version& other)
{
	return *this > other || *this == other;
}
