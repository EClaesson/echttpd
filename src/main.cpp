#include <iostream>

#include "Banner.hpp"
#include "Version.hpp"
#include "http/HttpServer.hpp"


#ifdef ECHTTPD_TEST
#include <gtest/gtest.h>
#include "../test/_tests.hpp"
#endif // ECHTTPD_TEST

int main(int argc, char* argv[])
{
#ifndef ECHTTPD_TEST
	std::cout << echttpd::Banner("echttpd v" + echttpd::Version::current.getAsString()).getAsString();

	echttpd::http::HttpServer server("", 8080, "./www");
	server.start();
#else
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
#endif // ECHTTPD_TEST
}
