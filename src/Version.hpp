#pragma once

#include <string>

namespace echttpd
{
	/**
	 * \brief Semantic version representation.
	 */
	class Version
	{
		private:
			int m_major, m_minor, m_patch;

		public:
			/**
			 * \brief Current version.
			 */
			static Version current;

			/**
			 * \param[in] major Major version number.
			 * \param[in] minor Minor version number.
			 * \param[in] patch Patch version number.
			 */
			explicit Version(int major, int minor, int patch);
			Version(const Version& other) = default;
			Version(Version&& other) = default;
			~Version() = default;

			/**
			 * \brief Get the major version number.
			 */
			int getMajor() const;

			/**
			 * \brief Get the minor version number.
			 */
			int getMinor() const;

			/**
			 * \brief Get the patch version number.
			 */
			int getPatch() const;

			/**
			 * \brief Get the version as a string.
			 * \return The version as a string of format "major.minor.patch".
			 */
			std::string getAsString() const;

			bool operator==(const Version& other);
			bool operator!=(const Version& other);
			bool operator<(const Version& other);
			bool operator<=(const Version& other);
			bool operator>(const Version& other);
			bool operator>=(const Version& other);
	};
}
