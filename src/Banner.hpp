#pragma once

#include <iostream>
#include <string>

namespace echttpd
{
	/**
	 * \brief Startup banner.
	 */
	class Banner
	{
	private:
		std::string m_text;
		char m_border_char;
		unsigned short m_cols;

	public:
		/**
		 * \param[in] text The text to put on the banner.
		 * \param[in] border The character to use as border.
		 */
		explicit Banner(const std::string& text, const char border = '#', const unsigned short cols = 60);
		Banner(const Banner& other) = default;
		Banner(Banner&& other) = default;
		~Banner() = default;

		/**
		 * \brief Get a string representation of the banner.
		 */
		std::string getAsString() const;
	};
}
