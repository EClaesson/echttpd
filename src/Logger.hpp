#pragma once

#include <string>

namespace echttpd
{
	/**
	 * \brief Logging facility.
	 */
	class Logger
	{
	private:
		std::string m_name;

		void mm_output(const std::string& title, const std::string& msg) const;

	public:
		Logger();
		Logger(const std::string& name);
		Logger(const Logger& other) = default;
		~Logger() = default;

		/**
		 * \brief Write a debug message. Will not output if DEBUG is not defined.
		 * \param[in] msg Message to write.
		 */
		void debug(const std::string& msg) const;

		/**
		 * \brief Write an info message.
		 * \param[in] msg Message to write.
		 */
		void info(const std::string& msg) const;

		/**
		 * \brief Write a warning message.
		 * \param[in] msg Message to write.
		 */
		void warning(const std::string& msg) const;

		/**
		 * \brief Write an error message. Non-fatal, program will continue running.
		 * \param[in] msg Message to write.
		 */
		void error(const std::string& msg) const;

		/**
		 * \brief Write a fatal error message. Program will exit.
		 * \param[in] msg Message to write.
		 */
		void fatal(const std::string& msg) const;

		/**
		 * \brief Get a named logger instance.
		 */
		static Logger getLogger(const std::string& name);
	};
}
