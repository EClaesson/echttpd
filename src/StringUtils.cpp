#include "StringUtils.hpp"

#include <sstream>

std::vector<std::string> echttpd::StringUtils::split(const std::string& str, const char delimiter, const unsigned int max_splits)
{
	std::vector<std::string> parts;
	std::string this_part;
	std::stringstream remaining;
	std::stringstream stream(str);

	while(std::getline(stream, this_part, delimiter))
	{
		if(max_splits == 0 || parts.size() < max_splits)
		{
			parts.push_back(this_part);
		}
		else
		{
			remaining << this_part << delimiter;
		}
	}

	std::string remaining_str = remaining.str();
	if(remaining_str.length() > 0)
	{
		parts.push_back(remaining_str.substr(0, remaining_str.length() - 1));
	}

	return parts;
}

std::string echttpd::StringUtils::trimLastIf(const std::string& str, const char chr)
{
	if(str.back() == chr)
	{
		return str.substr(0, str.length() - 1);
	}
	else
	{
		return str;
	}
}
