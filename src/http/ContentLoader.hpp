#pragma once

#include <string>

#include "LoadResult.hpp"

namespace echttpd
{
	namespace http
	{
		/**
		 * \brief Base class for content loaders.
		 */
		class ContentLoader
		{
		public:
			virtual ~ContentLoader() { };

			/**
			 * \brief Load the content item identified by the URI.
			 * \param]in] uri The uri whose content to load.
			 */
			virtual LoadResult loadByUri(const std::string& uri) = 0;
		};
	}
}
