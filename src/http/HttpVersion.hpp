#pragma once

#include <string>

namespace echttpd
{
	namespace http
	{
		/**
		 * \brief HTTP protocol version.
		 */
		enum class HttpVersion
		{
			NONE,
			v1_0,
			v1_1,
		};

		/**
		 * \brief Gets a HttpVersion from a string in format HTTP/x.y.
		 * \param[in] str The string to parse.
		 */
		HttpVersion httpVersionFromString(const std::string& str);

		/**
		 * \brief Convert a HttpVersion to a string of format HTTP/x.y.
		 * \param[in] version The version to convert.
		 */
		std::string httpVersionToString(const HttpVersion version);
	}
}
