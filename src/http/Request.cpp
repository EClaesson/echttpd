#include "Request.hpp"

#include <sstream>

#include "../StringUtils.hpp"

echttpd::Logger echttpd::http::Request::ms_logger = echttpd::Logger::getLogger("Request");

std::map<std::string, std::string> echttpd::http::Request::mms_parse_headers(const std::vector<std::string>& lines)
{
	std::map<std::string, std::string> headers;

	for(std::vector<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it)
	{
		if((*it).length() > 2)
		{
			auto elems = StringUtils::split(*it, ':', 1);

			if(elems.size() != 2)
			{
				ms_logger.error("Invalid header. Skipping.");
				continue;
			}

			auto key = elems[0];
			auto value = elems[1].substr(1);

			std::stringstream header_msg_ss;
			header_msg_ss << "  * " << key << " = " << value;
			ms_logger.debug(header_msg_ss.str());

			headers[key] = value;
		}
	}

	return headers;
}

echttpd::http::Request::Request() : m_method(HttpMethod::NONE), m_uri(""), m_http_version(HttpVersion::NONE), m_valid(false)
{

}

echttpd::http::Request::Request(const echttpd::http::HttpMethod method, const std::string& uri, const echttpd::http::HttpVersion version,
					const std::map<std::string, std::string>& headers) : m_method(method), m_uri(uri), m_http_version(version),
							m_headers(headers), m_valid(true)
{

}

echttpd::http::Request echttpd::http::Request::fromString(const std::string& data)
{
	ms_logger.debug("Parsing request");

	Request request;

	auto lines = echttpd::StringUtils::split(data, '\n');

	if(lines.size() < 1)
	{
		ms_logger.error("Invalid request data");
		return request;
	}

	// Handle request line
	auto request_line_elems = StringUtils::split(StringUtils::trimLastIf(lines[0], 0xD), ' ');

	if(request_line_elems.size() != 3)
	{
		ms_logger.error("Invalid request line");
		return request;
	}

	if(request_line_elems[0] == "GET")
	{
		request.m_method = HttpMethod::GET;
	}
	else if(request_line_elems[0] == "POST")
	{
		request.m_method = HttpMethod::POST;
	}
	else
	{
		ms_logger.error("Unsupported request method");
		return request;
	}

	request.m_uri = request_line_elems[1];

	request.m_http_version = httpVersionFromString(request_line_elems[2]);
	if(request.m_http_version == HttpVersion::NONE)
	{
		ms_logger.error("Unsupported HTTP version");
		return request;
	}

	// Parse headers
	ms_logger.debug("Parsing headers");
	lines.erase(lines.begin());
	request.m_headers = mms_parse_headers(lines);

	request.m_valid = true;
	return request;
}

echttpd::http::HttpMethod echttpd::http::Request::getMethod() const
{
	return m_method;
}

std::string echttpd::http::Request::getURI() const
{
	return m_uri;
}

echttpd::http::HttpVersion echttpd::http::Request::getHttpVersion() const
{
	return m_http_version;
}

bool echttpd::http::Request::hasHeader(const std::string& key) const
{
	return m_headers.count(key) > 0;
}

std::string echttpd::http::Request::getHeader(const std::string& key)
{
	return m_headers.at(key);
}

bool echttpd::http::Request::isValid() const
{
	return m_valid;
}
