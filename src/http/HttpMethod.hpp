#pragma once

namespace echttpd
{
	namespace http
	{
		/**
		 * \brief HTTP request method.
		 */
		enum class HttpMethod
		{
			NONE,
			GET,
			POST,
		};
	}
}
