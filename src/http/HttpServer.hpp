#pragma once

#include "HttpConnection.hpp"
#include "../Logger.hpp"
#include "FileLoader.hpp"

#include <string>
#include <memory>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

#define MAX_LISTEN_BACKLOG 1000

namespace echttpd
{
	namespace http
	{
		/**
		 * \brief Simple HTTP server.
		 */
		class HttpServer
		{
		private:
			echttpd::Logger m_logger;
			std::string m_bind_address;
			int m_port;
			std::string m_content_root;
			addrinfo* m_server_addrinfo;
			FileLoader* m_file_loader;

		public:
			/**
			 * \param[in] bind_address Address to bind to, empty for any.
			 * \param[in] port Port to listen to.
			 * \param[in] max_connections Maximum number of connections to allow.
			 */
			explicit HttpServer(const std::string& bind_address = "", const short port = 8080, const std::string& content_root = "./www");
			HttpServer(const HttpServer& other) = default;
			~HttpServer();

			/**
			 * \brief Start listening for connections.
			 */
			void start();
		};
	}
}
