#include "HttpServer.hpp"

#include <iostream>
#include <sstream>

#include <netinet/in.h>
#include <cstring>
#include <unistd.h>

echttpd::http::HttpServer::HttpServer(const std::string& bind_address, const short port, const std::string& content_root)
{
	m_logger = Logger::getLogger("HttpServer");

	m_bind_address = bind_address;
	m_port = port;
	m_content_root = content_root;
	m_server_addrinfo = NULL;
	m_file_loader = new FileLoader(m_content_root);
}

echttpd::http::HttpServer::~HttpServer()
{
	freeaddrinfo(m_server_addrinfo);
	delete m_file_loader;
}

void echttpd::http::HttpServer::start()
{
	m_logger.info("Starting...");

	addrinfo hints;
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	// Get address information
	m_logger.debug("Getting address information");
	if(getaddrinfo((m_bind_address.length() > 0 ? m_bind_address.c_str() : NULL), std::to_string(m_port).c_str(), &hints, &m_server_addrinfo) != 0)
	{
		m_logger.fatal("getaddrinfo() error");
	}

	// Create socket
	m_logger.debug("Creating socket");
	int socket_desc = socket(AF_INET, SOCK_STREAM, 0);
	if(socket_desc == -1)
	{
		std::cerr << "socket() error" << std::endl;
		return;
	}

	// Bind socket
	m_logger.debug("Binding socket");
	if(bind(socket_desc, m_server_addrinfo->ai_addr, m_server_addrinfo->ai_addrlen) != 0)
	{
		std::cerr << "bind() error" << std::endl;
		return;
	}

	// Start listening for incoming connections
	std::stringstream listen_msg_ss;
	listen_msg_ss << "Starting listening on " << (m_bind_address.length() > 0 ? m_bind_address : "*") << ":" << m_port;
	m_logger.debug(listen_msg_ss.str());
	if(listen(socket_desc, MAX_LISTEN_BACKLOG) != 0)
	{
		std::cerr << "listen() error" << std::endl;
		return;
	}

	while(true)
	{
		sockaddr_in client_addr;
		socklen_t addrlen = sizeof(client_addr);
		int client_id = accept(socket_desc, (sockaddr*)&client_addr, &addrlen);
		echttpd::http::HttpConnection connection(client_id, client_addr, m_file_loader);

		std::stringstream accept_msg_ss;
		accept_msg_ss << "Accepted connection from " << connection.getIP() << ":" << connection.getPort();
		m_logger.info(accept_msg_ss.str());

		connection.handle();
	}

	m_logger.info("Shutting down...");
}
