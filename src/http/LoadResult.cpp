#include "LoadResult.hpp"

echttpd::http::LoadResult::LoadResult() : m_successful(false), m_data("")
{

}

echttpd::http::LoadResult::LoadResult(const bool successful, const std::string& data) : m_successful(successful), m_data(data)
{

}

bool echttpd::http::LoadResult::isSuccessful() const
{
	return m_successful;
}

std::string echttpd::http::LoadResult::getData() const
{
	return m_data;
}
