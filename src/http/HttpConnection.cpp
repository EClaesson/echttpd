#include "HttpConnection.hpp"

#include <sstream>
#include <cstring>

#include <arpa/inet.h>
#include <unistd.h>

#include "Request.hpp"

echttpd::http::HttpConnection::HttpConnection(const int client_id, const sockaddr_in client_addr, FileLoader* file_loader) :
	m_id(client_id), m_addr(client_addr), m_file_loader(file_loader)
{
	std::stringstream logger_name_ss;
	logger_name_ss << "HttpConnection (" << m_id << ")";
	m_logger = Logger::getLogger(logger_name_ss.str());
}

int echttpd::http::HttpConnection::getId() const
{
	return m_id;
}

unsigned short echttpd::http::HttpConnection::getPort() const
{
	return m_addr.sin_port;
}

std::string echttpd::http::HttpConnection::getIP() const
{
	char buffer[INET_ADDRSTRLEN];
	inet_ntop(AF_INET, &m_addr.sin_addr, buffer, INET_ADDRSTRLEN);
	return buffer;
}

void echttpd::http::HttpConnection::closeConnection()
{
	shutdown(m_id, SHUT_RDWR);
	close(m_id);
}

void echttpd::http::HttpConnection::handle()
{
	m_logger.debug("Handling connection");

	// Fork process and handle connection
	if(fork() == 0)
	{
		char msg_buffer[MESSAGE_BUFFER_SIZE];
		std::string msg;
		int bytes_received;

		memset((void*)msg_buffer, (int)'\0', MESSAGE_BUFFER_SIZE);
		bytes_received = recv(m_id, msg_buffer, MESSAGE_BUFFER_SIZE, 0);
		msg = msg_buffer;

		if(bytes_received < 0)
		{
			m_logger.error("Failed to read from connection");
		}
		else if(bytes_received == 0)
		{
			m_logger.error("Connection closed unexpectedly");
		}
		else
		{
			std::stringstream received_msg_ss;
			received_msg_ss << "Received: \"" << msg << "\"";
			m_logger.debug(received_msg_ss.str());

			auto request = echttpd::http::Request::fromString(msg);

			std::stringstream request_msg_ss;
			request_msg_ss << "Request: " << request.getURI();
			m_logger.info(request_msg_ss.str());

			if(!request.isValid())
			{
				writeResponse(Response(request, 400, "Error 400: Bad Request"));
				return;
			}

			auto loadResult = m_file_loader->loadByUri(request.getURI());

			if(!loadResult.isSuccessful()) writeResponse(Response(request, 404, "Error 404: Not Found"));
			else writeResponse(Response(request, 200, loadResult.getData()));
		}

		m_logger.debug("Request handled");

		closeConnection();
		exit(0);
	}
}

void echttpd::http::HttpConnection::writeResponse(const Response& response) const
{
	std::string data = response.toStringData();
	std::string body = response.getBody();

	std::stringstream ss;
	ss << "  * Response: " << response.getStatusCode() << " " << response.getStatusMessage();
	m_logger.info(ss.str());

	write(m_id, data.c_str(), data.length());
	write(m_id, body.c_str(), body.length());
}
