#include "Response.hpp"

#include <sstream>

std::map<unsigned short, std::string> echttpd::http::Response::ms_status_messages = {
		{200, "OK"},
		{400, "Bad Request"},
		{404, "Not found"},
};

echttpd::http::Response::Response(const Request& request, const unsigned short status_code, const std::string& body) :
		m_status_code(status_code), m_http_version(request.getHttpVersion()), m_body(body)
{

}

unsigned short echttpd::http::Response::getStatusCode() const
{
	return m_status_code;
}

std::string echttpd::http::Response::getStatusMessage() const
{
	if(ms_status_messages.count(m_status_code) > 0)
	{
		return ms_status_messages[m_status_code];
	}
	else
	{
		return "";
	}
}

echttpd::http::HttpVersion echttpd::http::Response::getHttpVersion() const
{
	return m_http_version;
}

std::string echttpd::http::Response::getBody() const
{
	return m_body;
}

std::string echttpd::http::Response::toStringData() const
{
	std::stringstream ss;
	ss << httpVersionToString(m_http_version) << " " << m_status_code << " " << getStatusMessage() << "\n\n";
	return ss.str();
}
