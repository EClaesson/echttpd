#include "HttpVersion.hpp"

echttpd::http::HttpVersion echttpd::http::httpVersionFromString(const std::string& str)
{
	if(str == "HTTP/1.0") return HttpVersion::v1_0;
	else if(str == "HTTP/1.1") return HttpVersion::v1_1;
	else return HttpVersion::NONE;
}

std::string echttpd::http::httpVersionToString(const echttpd::http::HttpVersion version)
{
	if(version == HttpVersion::v1_0) return "HTTP/1.0";
	else if(version == HttpVersion::v1_1) return "HTTP/1.1";
	else return "";
}
