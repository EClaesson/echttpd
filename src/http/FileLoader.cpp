#include "FileLoader.hpp"

#include <fstream>
#include <sstream>

echttpd::http::FileLoader::FileLoader(const std::string& content_root) : m_content_root(content_root)
{

}

echttpd::http::LoadResult echttpd::http::FileLoader::loadByUri(const std::string& uri)
{
	std::stringstream path_ss;
	path_ss << m_content_root << (uri != "/" ? uri : "/index.html");

	std::ifstream file_stream(path_ss.str());
	std::stringstream stream;

	if(!file_stream.is_open())
	{
		return LoadResult(false, "");
	}

	while(file_stream >> stream.rdbuf());

	return LoadResult(true, stream.str());
}
