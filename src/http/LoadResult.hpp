#pragma once

#include <string>

namespace echttpd
{
	namespace http
	{
		/**
		 * \brief The result of a content load operation.
		 */
		class LoadResult
		{
		private:
			bool m_successful;
			std::string m_data;

		public:
			LoadResult();

			/**
			 * \param[in] successful Whether the operation was successful.
			 * \param[in] data The loaded data.
			 */
			LoadResult(const bool successful, const std::string& data);
			LoadResult(const LoadResult& other) = default;
			LoadResult(LoadResult&& other) = default;
			~LoadResult() = default;

			/**
			 * \brief Checks if the operation was successful.
			 */
			bool isSuccessful() const;

			/**
			 * \brief Gets the content data.
			 */
			std::string getData() const;
		};
	}
}
