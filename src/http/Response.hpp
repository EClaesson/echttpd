#pragma once

#include <string>
#include <map>

#include "HttpVersion.hpp"
#include "Request.hpp"

namespace echttpd
{
	namespace http
	{
		/**
		 * \brief Response from server to client.
		 */
		class Response
		{
		private:
			static std::map<unsigned short, std::string> ms_status_messages;

			unsigned short m_status_code;
			HttpVersion m_http_version;
			std::string m_body;

		public:
			/**
			 * \param[in] request The originating request.
			 * \param[in] status_code The HTTP status code to send.
			 * \param[in] body The body to send.
			 */
			explicit Response(const Request& request, const unsigned short status_code, const std::string& body);
			Response(const Response& other) = default;
			Response(Response&& other) = default;
			~Response() = default;

			/**
			 * \brief Gets the status code.
			 */
			unsigned short getStatusCode() const;

			/**
			 * \brief Gets a text representation of the current status code.
			 */
			std::string getStatusMessage() const;

			/**
			 * \brief Gets the used HTTP protocol version.
			 */
			HttpVersion getHttpVersion() const;

			/**
			 * \brief Gets the body data.
			 */
			std::string getBody() const;

			/**
			 * \brief Formats the response into a HTTP response.
			 */
			std::string toStringData() const;
		};
	}
}
