#pragma once

#include <map>
#include <vector>

#include "../Logger.hpp"
#include "HttpVersion.hpp"
#include "HttpMethod.hpp"

namespace echttpd
{
	namespace http
	{
		/**
		 * \brief HTTP request.
		 */
		class Request
		{
		private:
			static Logger ms_logger;

			HttpMethod m_method;
			std::string m_uri;
			HttpVersion m_http_version;
			std::map<std::string, std::string> m_headers;
			bool m_valid;

			static std::map<std::string, std::string> mms_parse_headers(const std::vector<std::string>& lines);

		public:
			Request();
			Request(const HttpMethod method, const std::string& uri, const HttpVersion version,
					const std::map<std::string, std::string>& headers);
			Request(const Request& other) = default;
			Request(Request&& other) = default;
			~Request() = default;

			/**
			 * \brief Parses a raw HTTP request.
			 * \param[in] data The data to parse.
			 */
			static Request fromString(const std::string& data);

			/**
			 * \brief Gets the requests method.
			 */
			HttpMethod getMethod() const;

			/**
			 * \brief Gets the requested URI.
			 */
			std::string getURI() const;

			/**
			 * \brief Gets the used HTTP protocol version.
			 */
			HttpVersion getHttpVersion() const;

			/**
			 * \brief Checks whether a certain header was received.
			 * \param[in] key Name of the header.
			 */
			bool hasHeader(const std::string& key) const;

			/**
			 * \brief Gets the content of a header.
			 * \param[in] key The name of the header.
			 */
			std::string getHeader(const std::string& key);

			/**
			 * \brief Checks if the request was valid and fully parsed.
			 */
			bool isValid() const;
		};
	}
}




