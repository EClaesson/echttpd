#pragma once

#include <string>

#include "ContentLoader.hpp"

namespace echttpd
{
	namespace http
	{
		/**
		 * \brief Content loader that reads files on disk.
		 */
		class FileLoader : ContentLoader
		{
		private:
			std::string m_content_root;

		public:
			/**
			 * \param[in] content_root The root directory in which to look for files.
			 */
			explicit FileLoader(const std::string& content_root);
			virtual ~FileLoader() = default;

			virtual LoadResult loadByUri(const std::string& uri);
		};
	}
}
