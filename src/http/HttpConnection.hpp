#pragma once

#include <string>
#include <memory>

#include <netinet/in.h>

#include "../Logger.hpp"
#include "Response.hpp"
#include "FileLoader.hpp"

#define MESSAGE_BUFFER_SIZE 4096

namespace echttpd
{
	namespace http
	{
		/**
		 * \brief A client connection.
		 */
		class HttpConnection
		{
		private:
			int m_id;
			sockaddr_in m_addr;
			Logger m_logger;
			FileLoader* m_file_loader;

		public:
			/**
			 * \param[in] client_id The client connections socket descriptor.
			 * \param[in] client_addr The client connections address.
			 * \param[in] The FileLoader to use to load content for the client.
			 */
			explicit HttpConnection(const int client_id, const sockaddr_in client_addr, FileLoader* file_loader);
			HttpConnection(const HttpConnection& other) = default;
			HttpConnection(HttpConnection&& other) = default;
			~HttpConnection() = default;

			/**
			 * \brief Gets the socket descriptor.
			 */
			int getId() const;

			/**
			 * \brief Get the port the client is connected through.
			 */
			unsigned short getPort() const;

			/**
			 * \brief Gets the clients IP address.
			 */
			std::string getIP() const;

			/**
			 * \brief Close the connection to the client.
			 */
			void closeConnection();

			/**
			 * \brief Handle a single request and close the connection.
			 */
			void handle();

			/**
			 * \brief Write a response object to the client.
			 * \param[in] response The response to write.
			 */
			void writeResponse(const Response& response) const;
		};
	}
}
